.PHONY: test ssh

IMAGE_NAME=micro8734/configuration-server

run:
	$(info Starting in local environment)
	@ make _docker-compose env=local command='up --build --remove-orphans -d'

stop:
	$(info Stoping local containers)
	@ make _docker-compose env=local command='down -v'

ssh:
	$(info Attach shell to tty)
	@ make _docker-compose env=local command="exec apartmenmt bash"

test:
	$(info Starting in test environment)
	@ make _docker-compose env=test command='up --build --remove-orphans'

_docker-compose:
	@ docker-compose -f docker-compose.yml  -f compose-overrides/docker-compose.$(env).yml $(command)

build/docker:
	$(info Building image $(IMAGE_NAME))

	@ docker image build -t $(IMAGE_NAME) .

deploy/production:
	$(info Deploy to production)

deploy/staging:
	$(info Deploy to staging)
